#include "mpi.h"    //mpi calls
#include <iostream> //cout, cerr
using namespace std;

//CONSTANTS
#define MATRIX_SIZE 4

//global matrices
int* a;
int* b;
int* ab;
int* as;
int* bs;
int* _abs;

/** MPI program to multiply two matrices in a distributed way*/
int main(int argc, char* argv[]) {
  //initialize the MPI environment
  int size, rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  //divide the work uniformly between processes
  const unsigned int wp = (MATRIX_SIZE * MATRIX_SIZE) / size;

  //
  if (rank == 0) {
    //
    a = new int[MATRIX_SIZE * MATRIX_SIZE];
    b = new int[MATRIX_SIZE * MATRIX_SIZE];
    ab = new int[MATRIX_SIZE * MATRIX_SIZE];

    //
    for (int i = 0; i < MATRIX_SIZE; i++) {
      for (int j = 0; j < MATRIX_SIZE; j++) {
	a[(i * MATRIX_SIZE) + j] = i + j + 1;
	b[(i * MATRIX_SIZE) + j] = (i * j) + 1;
      }
    }
  }

  //
  as  = new int[wp];
  bs  = new int[wp];
  _abs = new int[wp];
  MPI_Scatter(a, wp , MPI_INT, as, wp, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Scatter(b, wp, MPI_INT, bs, wp, MPI_INT, 0, MPI_COMM_WORLD);

  //
  for (unsigned int i = 0; i < wp; i++) {
    _abs[i] = as[i] + bs[i];
  }

  //
  MPI_Gather(_abs, wp, MPI_INT, ab, wp, MPI_INT, 0, MPI_COMM_WORLD);

  //
  if (rank == 0) {
    cout << "a" << endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
      for (int j = 0; j < MATRIX_SIZE; j++) {
	      cout << a[(i * MATRIX_SIZE) + j] << " | ";
      }
      cout << endl;
    }
    cout << "b" << endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
      for (int j = 0; j < MATRIX_SIZE; j++) {
	      cout << b[(i * MATRIX_SIZE) + j] << " | ";
      }
      cout << endl;
    }
    cout << "ab" << endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
      for (int j = 0; j < MATRIX_SIZE; j++) {
	      cout << ab[(i * MATRIX_SIZE) + j] << " | ";
      }
      cout << endl;
    }
  }

  //end program
  MPI_Finalize();
  return 0;
}
