#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#define NUM_ITERATIONS 3
using namespace std;

//shared variables
int a_to_b_data;
mutex a_to_b_mutex;
bool a_to_b_condition = false;
condition_variable a_to_b_cv;

double b_to_c_data;
mutex b_to_c_mutex;
bool b_to_c_condition = false;
condition_variable b_to_c_cv;

//A
void producer() {
  unique_lock<mutex> lock(a_to_b_mutex, defer_lock);
  for (int i = 0; i < NUM_ITERATIONS; i++) {
    //wait until B is ready to process data
    lock.lock();
    a_to_b_cv.wait(lock, []{return !a_to_b_condition;});
    //get the data
    int input;
    cout << "Enter a value: " << flush;
    cin >> input;
    //send the data to B
    a_to_b_data = input;
    //signal B that the data is ready
    a_to_b_condition = true;
    lock.unlock();
    a_to_b_cv.notify_one();
  }
}

//B
void intermediate() {
  unique_lock<mutex> lock_a(a_to_b_mutex, defer_lock);
  unique_lock<mutex> lock_c(b_to_c_mutex, defer_lock);
  for (int i = 0; i < NUM_ITERATIONS; i++) {
    //wait until A has sent the data
    lock_a.lock();
    a_to_b_cv.wait(lock_a, []{return a_to_b_condition;});
    //get the data from A
    int a_data = a_to_b_data;
    //process the data
    double c_data = ((double)a_data)/2.0d;
    //wait until C is ready to process data
    lock_c.lock();
    b_to_c_cv.wait(lock_c, []{return !b_to_c_condition;});
    //send the data to C
    b_to_c_data = c_data;
    //signal C that the data is ready
    b_to_c_condition = true;
    lock_c.unlock();
    b_to_c_cv.notify_one();
    //notify A that B is ready to receive data again
    a_to_b_condition = false;
    lock_a.unlock();
    a_to_b_cv.notify_one();
  }
}


//C
void consumer() {
  unique_lock<mutex> lock(b_to_c_mutex, defer_lock);
  for (int i = 0; i < NUM_ITERATIONS; i++) {
    //wait until B has sent the data
    lock.lock();
    b_to_c_cv.wait(lock, []{return b_to_c_condition;});
    //get the data from B
    double data = b_to_c_data;
    //print the received data
    cout << "The result is: " << data << endl << flush;
    //notify B that C is ready to receive data again
    b_to_c_condition = false;
    lock.unlock();
    b_to_c_cv.notify_one();
  }
}


//Application entry point
int main() {
  cout << "Assembly line started" << endl;
  thread ta(producer);
  thread tb(intermediate);
  thread tc(consumer);
  ta.join();
  tb.join();
  tc.join();
  cout << "Assembly line stoped" << endl;
  return 0;
}
