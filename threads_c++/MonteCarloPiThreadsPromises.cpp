#include <iostream>
#include <future>
#include <thread>
#include <random>
using namespace std;

long long total = 100000000;

/**
 *
 */
void mc_count(long long iterations, promise<long long> result) {
  long long count = 0;
  random_device r;
  default_random_engine random(r());
  uniform_real_distribution<double> uniform_dist(0.0, 1.0);

  for (int i = 0; i < iterations; i++) {
    double x = uniform_dist(random);
    double y = uniform_dist(random);
    if ((x*x + y*y) < 1.0) {
      count++;
    }
  }

  result.set_value(count);
}

/**
 *
 */
int main() {
  //
  future<long long> promises[NUM_THREADS];
  long long countPerThread = total / NUM_THREADS;
  for (int i = 0; i < NUM_THREADS; i++) {
    //
    promise<long long> thread_promise;
    promises[i] = thread_promise.get_future();
    thread t(mc_count, countPerThread, move(thread_promise));
    t.detach();
  }

  //
  long long count = 0;
  for (int i = 0; i < NUM_THREADS; i++) {
    count += promises[i].get();
  }

  //
  long double pi = (long double)4.0 * ((long double)count/(long double)total);
  cout << "pi was estimated as " << pi << endl;
}
