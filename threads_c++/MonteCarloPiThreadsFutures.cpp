#include <iostream>
#include <future>
#include <random>
using namespace std;

long long total = 100000000;

/**
 *
 */
long long mcCount(long long iterations) {
  long long count = 0;
  random_device r;
  default_random_engine random(r());
  uniform_real_distribution<double> uniform_dist(0.0, 1.0);

  for (int i = 0; i < iterations; i++) {
    double x = uniform_dist(random);
    double y = uniform_dist(random);
    if ((x*x + y*y) < 1.0) {
      count++;
    }
  }

  return count;
}

/**
 *
 */
int main() {
  //
  future<long long> futures[NUM_THREADS];
  long long countPerThread = total / NUM_THREADS;
  for (int i = 0; i < NUM_THREADS; i++) {
    //
    //futures[i] = async(mcCount, countPerThread);
    futures[i] = async(launch::async, mcCount, countPerThread);
  }

  //
  long long count = 0;
  for (int i = 0; i < NUM_THREADS; i++) {
    count += futures[i].get();
  }

  //
  double pi = 4.0 * ((double)count/(double)total);
  cout << "pi was estimated as " << pi << endl;
}
