#include <iostream>
#include <thread>
using namespace std;

//This function will be called from a thread
void helloWorld(int tid) {
  cout << "Hello World from thread " << tid << endl;
}

/**
 *
 */
int main() {
  //launch the thread
  thread threads[NUM_THREADS];
  for (int i = 0; i < NUM_THREADS; i++) {
    threads[i] = thread(helloWorld, i);
  }

  //join the thread with the main thread
  for (int i = 0; i < NUM_THREADS; i++) {
    threads[i].join();
  }

  //end
  cout << "root ends" << endl;
  return 0;
}
