#include <iostream>
#include <future>
using namespace std;

//
long size = 8;
long double * v1;
long double * v2;

/**
 *
 */
long double dotProduct (long start, long end) {
  long double sum = 0;
  for (long i = start; i < end; i++) sum += v1[i] * v2[i];
  return sum;
}

/**
 *
 */
int main() {
  //
  v1 = new long double[size];
  v2 = new long double[size];
  for (long i = 0; i < size; i++) {
    v1[i] = 2 * i;
    v2[i] = (3 * i) + 1;
  }

  //
  future<long double> futures[NUM_THREADS];
  long step = (size / NUM_THREADS) + 1;
  long start = 0;
  long end = step;
  for (int i = 0; i < NUM_THREADS; i++) {
    //
    futures[i] = async(launch::async, dotProduct, start, end);
    start = end;
    end += step;
    if (end >= size) end = size; //
  }

  //
  long double sum = 0;
  for (int i = 0; i < NUM_THREADS; i++) {
    sum += futures[i].get();
  }
  cout << "The dot product was " << sum << endl;
}
